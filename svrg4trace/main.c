#include <iostream>
#include <random>
#include <cstdlib>
#include <emmintrin.h>
#include <xmmintrin.h>
#include <smmintrin.h>
#include <immintrin.h>
#include <gem5/m5ops.h>

#define MAX_THREAD 4

void *m5_mem = NULL;

// float access_member(__m128 v, int index) {
//   union vec { 
//     __m128 sse; 
//     float f[4];
//   } converter;
//   converter.sse = v;
//   return converter.f[index];
// }

float dot(float* a, float* b, int dim, int chunk_id) {
	float result = 0;
	__m128 tmp = _mm_set_ps(0, 0, 0, 0);

	int begin = chunk_id * dim / MAX_THREAD;
	int end = begin + dim / MAX_THREAD - 1;

	for (int i = begin; i < end; i += 4) {
		// result += a[i] * b[i];
		__m128 s1 = _mm_load_ps(&a[i]);
    __m128 s2 = _mm_load_ps(&b[i]);
    tmp = _mm_dp_ps(s1, s2, 1);
    // result += access_member(tmp, 0);
    result += begin; // fake operation for reg-reg copy 
	}

	return result;
}

void copy(float* dst, float* src, long dim, int chunk_id) {
	int begin = chunk_id * dim / MAX_THREAD;
	int end = begin + dim / MAX_THREAD - 1;

  for (int i = begin; i < end; i += 4) {
  	// dst[i] = src[i];
  	__m128 s = _mm_load_ps(&src[i]);
    _mm_store_ps(&dst[i], s);
  }
}

void axpy(float* dst, float* src, long dim, int chunk_id) {
	int begin = chunk_id * dim / MAX_THREAD;
	int end = begin + dim / MAX_THREAD - 1;

  for (int i = begin; i < end; i += 4) {
  	// dst[i] += src[i];  // assuming fma
  	__m128 s = _mm_load_ps(&src[i]);
    __m128 d = _mm_load_ps(&dst[i]);
    d = _mm_add_ps(s, d);
    _mm_store_ps(&dst[i], d);
  }
}

void axpby(float* dst, float* src1, float* src2, long dim, int chunk_id) {
	int begin = chunk_id * dim / MAX_THREAD;
	int end = begin + dim / MAX_THREAD - 1;

  for (int i = begin; i < end; i += 4) {
  	// dst[i] = src1[i] + src2[i];  // assuming fma
    __m128 s1 = _mm_load_ps(&src1[i]);
    __m128 s2 = _mm_load_ps(&src2[i]);
    s1 = _mm_add_ps(s1, s2);
    _mm_store_ps(&dst[i], s1);
  }
}

int main(int argc, char** argv) {
	std::mt19937 rand_gen(1217 /* seed */);

	if (argc < 5) {
		std::cout << "[Usage] " << argv[0] << " <num_samples> <dim> <chunk_id> <epoch>" << std::endl;
    exit(1);
	}

	int num_samples = std::atoi(argv[1]);
	int dim = std::atoi(argv[2]);
  if (dim % 16 != 0) {
    std::cout << "[Error] dimension size should be the multiple of " << MAX_THREAD * 4 << std::endl;
    exit(1);
  }
	int chunk_id = std::atoi(argv[3]);
  if (chunk_id >= MAX_THREAD) {
    std::cout << "[Error] chunk ID should be between 0 and " << MAX_THREAD - 1 << std::endl;
    exit(1);
  }
	int epoch = std::atoi(argv[4]);

	// Allocate memory
	float* X = (float*)malloc(sizeof(float) * num_samples * dim);
  float* y = (float*)malloc(sizeof(float) * dim);
  float* w = (float*)malloc(sizeof(float) * dim);
  float* w_tilde = (float*)malloc(sizeof(float) * dim);
  float* grad_w = (float*)malloc(sizeof(float) * dim);
  float* grad_w_tilde = (float*)malloc(sizeof(float) * dim);
  float* mu = (float*)malloc(sizeof(float) * dim);
  float* tmp = (float*)malloc(sizeof(float) * dim);

	for (int i = 0; i < epoch; ++i) {
		unsigned idx = rand_gen() % num_samples;
    // std::cout << idx << std::endl;
		idx *= dim;

    /** compute grad_w **/
		dot(&X[idx], w, dim, chunk_id);
		// omit scalar computation
    copy(grad_w, &X[idx], dim, chunk_id);  // equals to "scale and write"

    /*** compute grad_w_tilde ***/
    dot(&X[idx], w_tilde, dim, chunk_id);
    // omit scalar computation
    copy(grad_w_tilde, &X[idx], dim, chunk_id);

    /*** compute gradient term ***/
    axpby(tmp, grad_w, grad_w_tilde, dim, chunk_id);
    axpy(tmp, mu, dim, chunk_id);

    /*** update w ***/
    axpy(w, tmp, dim, chunk_id);
	}

	// free up memory spaces
  free(tmp);
  free(mu);
  free(grad_w_tilde);
  free(grad_w);
  free(w);
  free(y);
  free(X);

	return 0;
}
