#!/bin/bash

./build/X86/gem5.opt -d /home/bcho/hdd/benchmark/607.cacuBSSN_s/workdir/after_chkpt ./configs/example/fs.py --checkpoint-dir=/home/bcho/gem5/m5out/cpu2017_fs_ckpt --maxinsts 200000000 --mem-size=8GB --kernel=/home/bcho/hdd/gem5_fs/binaries/x86_64-vmlinux-2.6.22.9 --disk-image=/home/bcho/hdd/gem5_fs/disks/x86root.img --restore-with-cpu=AtomicSimpleCPU -r 1 --script=./configs/boot/607.cacuBSSN_s.rcS
