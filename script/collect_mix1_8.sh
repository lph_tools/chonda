#!/bin/bash

collect() {
  host_ops=`grep numOps $stat_file | awk '{s += $2}END{print s}'`
  bytes=`grep nda_bytes $stat_file | awk '{s += $2}END{print s}'`
  cycles=`grep numCycles $stat_file | awk -v max=0 '{if($2>max){max=$2}}END{print max} '`
  host_ipc=`echo "scale=2; $host_ops / $cycles" | bc 2> /dev/null`
  nda_bpc=`echo "scale=2; $bytes / $cycles" | bc 2> /dev/null`
  echo "mix$mix_id $config $host_ipc $nda_bpc"
}

# configs="nda_ddr4_disabled_pgclr nda_ddr4 nda_ddr4_sync_to_host"
# configs="nda_ddr4_disabled nda_ddr4 nda_ddr4_sync_to_host s2h_bf4_flip_coin s2h_bf8_flip_coin s2h_bf16_flip_coin"
configs="nda_ddr4_disabled nda_ddr4 nda_ddr4_sync_to_host s2h_bf8_flip_coin s2h_bf8_flip_coin_indep"
# configs="nda_ddr4_disabled nda_ddr4_random_trans nda_ddr4_s2h_random_trans"
bench_home="/home/bcho/hdd/benchmark"
mix_id=0
config=nda_ddr4

for i in {0..8}; do
  for cfg in $configs; do
    mix_id=$i
    config=$cfg
    stat_file="${bench_home}/mix${mix_id}/workdir/replay_etrace/${config}/stats.txt"
    collect
    # nda_bw
  done
done

