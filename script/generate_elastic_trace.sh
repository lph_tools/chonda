#!/bin/bash

gem5_home=/home/bcho/gem5

if [ -z "$1" ]; then
  echo "Need benchmark name"
  exit
else
  bench=$1
fi

bench_dir=$gem5_home/benchmark/$bench
if [ ! -d $bench_dir ]; then
  echo "$bench_dir does not exist"
  exit
fi

cd $bench_dir
mkdir -p workdir
WORKDIR=`sed -n 1p cmdline`
BIN=$WORKDIR/`sed -n 2p cmdline`
OPTS=`sed -n 3p cmdline`
FF=`sed -n 4p cmdline`
if [! -z $OPTS]; then
  OPTS="--options=$OPTS"
fi
ln -s $WORKDIR/* workdir
ln -s $BIN workdir/program
stdin_opt=`sed -n 5p cmdline`
if [ ! -z $stdin_opt ]; then
  stdin_opt="--input=$stdin_opt"
fi

cd workdir
$gem5_home/build/X86/gem5.opt --outdir=./m5out $gem5_home/configs/example/se.py --data-trace-file=deptrace.proto.gz --inst-trace-file=fetchtrace.proto.gz --elastic-trace-en --mem-size=8GB --fast-forward $FF --cpu-type=DerivO3CPU --caches --mem-type=SimpleMemory --maxinsts 1000000000 -c program $OPTS $stdin_opt | tee trace_gen.log 
