#!/bin/bash
 
if [ "$#" -ne 3 ]; then
  echo "[Usage] $0 app_name mem_config mix_id"
  echo " --> Example: $0 mix0 DDR4-config 0"
  exit 1
fi

gem5_home=/work/03528/bcho/stampede2/chonda
name=$1
mem_config=$2
mix_id=$3
app_name=`find $gem5_home/../benchmark/* -name "$name*" -exec sh -c 'echo $(basename {})' \;`

if [ -z "$app_name" ]; then
  echo "Application mix $app_name does not exist"
  exit 1
fi

if [ -z "$mem_config" ]; then
  echo "Specify memory configuration (e.g. DDR4-config)"
  exit 1
fi

cache_config="--caches --l2cache --cacheline_size=64 --l1d_size=32kB --l1d_assoc=4 --l2_size=4MB --l2_assoc=8 --l1i_size=32kB --l1i_assoc=4"
ramulator_config="--mem-type=Ramulator --ramulator-config=$gem5_home/configs/ramulator/${mem_config}.cfg"
outdir="$gem5_home/../benchmark/$app_name/workdir/replay_etrace/$mem_config"
etrace_config="--data-trace-file=system.switch_cpus.traceListener.deptrace.proto.gz --inst-trace-file=system.switch_cpus.traceListener.fetchtrace.proto.gz"
common_opts="--num-cpu=4 --mem-size=4GB --cpu-type=TraceCPU --maxinsts 200000000 --mix-id $mix_id" 

cmd=`echo "./build/X86/gem5.opt --outdir=$outdir $gem5_home/configs/example/etrace_replay_multicore.py $etrace_config $cache_config $ramulator_config $common_opts"`
echo $cmd
# eval $cmd
