#!/bin/bash

act_e=2.1 # nJ
nda_rw_e=0.1 # nJ
hst_rw_e=0.3 # nJ
nda_fp_e=0.02 # nJ
nda_buf_e=0.02 # nJ per 64-byte access
nda_buf_leak_p=0.011 # W
num_ndp=8

est_mem_power() {
  # NDA
  num_nda_act=`echo "$nda_bytes / 8192" | bc -l 2> /dev/null`
  num_nda_inst=`grep nda_insts $stat_file 2> /dev/null | awk '{s += $2}END{print s}' 2> /dev/null`
  nda_act_e=`echo "$num_nda_act * $act_e" | bc -l 2> /dev/null`
  nda_mem_e=`echo "$nda_bytes * $nda_rw_e" | bc -l 2> /dev/null`
  # echo "$nda_bytes * $nda_mem_e"
  nda_comp_e=`echo "$num_nda_inst * $nda_fp_e" | bc -l 2> /dev/null`
  nda_buffer_e=`echo "$num_nda_inst / 8 * $nda_buf_e" | bc -l 2> /dev/null`
  nda_e=`echo "$nda_act_e + $nda_mem_e + $nda_comp_e + $nda_buffer_e" | bc -l 2> /dev/null`
  nda_p=`echo "scale=2; $nda_e * 1.2 / $dram_cycle + $nda_buf_leak_p * $num_ndp" | bc -l 2> /dev/null` # mW
  time=`echo "$dram_cycle / 1.2" | bc -l 2> /dev/null` # ns
  # echo "num_nda_act: $num_nda_act, num_nda_bytes: $nda_bytes, num_nda_inst: $num_nda_inst"
  # echo "act_e: $nda_act_e, rw_e: $nda_mem_e, fp_e: $nda_comp_e, buf_e: $nda_buffer_e, time: $time"
  nda_act_p=`echo "$nda_act_e / $time" | bc -l 2> /dev/null`
  nda_mem_p=`echo "$nda_mem_e / $time" | bc -l 2> /dev/null`
  nda_comp_p=`echo "$nda_comp_e / $time" | bc -l 2> /dev/null`
  nda_buffer_p=`echo "$nda_buffer_e / $time" | bc -l 2> /dev/null`
  tot_nda_buf_leak_p=`echo "$nda_buf_leak_p * $num_ndp" | bc -l 2> /dev/null`
  # echo "act_p: $nda_act_p, rw_p: $nda_mem_p, fp_p: $nda_comp_p, buf_p: $nda_buffer_p, buf_leak_p: $tot_nda_buf_leak_p"

  # HOST
  num_rd=`grep read_transaction_bytes $stat_file 2> /dev/null | awk '{s += $2}END{print s}' 2> /dev/null`
  num_wr=`grep write_transaction_bytes $stat_file 2> /dev/null | awk '{s += $2}END{print s}' 2> /dev/null`
  num_rbhit=`grep "row_hits_channel_._core " $stat_file 2> /dev/null | awk '{s += $2}END{print s}' 2> /dev/null`
  num_host_bytes=`echo "$num_rd + $num_wr" | bc -l 2> /dev/null`
  num_host_act=`echo "$num_host_bytes / 64 - $num_rbhit" | bc -l 2> /dev/null`
  host_act_e=`echo "$num_host_act * $act_e" | bc -l 2> /dev/null`
  host_rw_e=`echo "$num_host_bytes * $hst_rw_e" | bc -l 2> /dev/null`
  host_act_p=`echo "$host_act_e / $time" | bc -l 2> /dev/null`
  host_rw_p=`echo "$host_rw_e / $time" | bc -l 2> /dev/null`
  host_p=`echo "scale=2; $host_act_p + $host_rw_p" | bc -l 2> /dev/null`

  # echo "num_rd=$num_rd, num_wr=$num_wr, num_rbhit=$num_rbhit"
  # echo "num_host_bytes=$num_host_bytes, num_host_act=$num_host_act"
  # echo "host_act_e=$host_act_e, host_rw_e=$host_rw_e"
  # echo "host_act_p=$host_act_p, host_rw_p=$host_rw_p"
}

collect() {
  host_ops=`grep numOps $stat_file 2> /dev/null | awk '{s += $2}END{print s}' 2> /dev/null`
  nda_bytes=`grep nda_bytes $stat_file 2> /dev/null | awk '{s += $2}END{print s}' 2> /dev/null`
  cycles=`grep numCycles $stat_file 2> /dev/null | awk -v max=0 '{if($2>max){max=$2}}END{print max} ' 2> /dev/null`
  ticks=`grep sim_ticks $stat_file 2> /dev/null | awk -v max=0 '{if($2>max){max=$2}}END{print max} ' 2> /dev/null`
  # tps=`grep host_tick_rate $stat_file 2> /dev/null | awk -v max=0 '{if($2>max){max=$2}}END{print max} ' 2> /dev/null`
  # sec=`echo "scale=2; $ticks / $tps" | bc -l 2> /dev/null`
  dram_cycle=`grep dram_cycles $stat_file 2> /dev/null | awk -v max=0 '{if($2>max){max=$2}}END{print max} ' 2> /dev/null`
  host_ipc=`echo "scale=2; $host_ops / $cycles" | bc -l 2> /dev/null`
  nda_bpc=`echo "scale=2; $nda_bytes / $cycles" | bc -l 2> /dev/null`
  nda_bw=`echo "scale=2; $nda_bytes / ($dram_cycle / 1.2)" | bc -l 2> /dev/null`
  est_mem_power
  echo "mix$mix_id,$config,$host_ipc,$nda_bw,$nda_p,$host_p"
}

# configs="nda_ddr4_disabled_pgclr nda_ddr4 nda_ddr4_sync_to_host"
# configs="nda_ddr4_disabled nda_ddr4 nda_ddr4_sync_to_host s2h_bf4_flip_coin s2h_bf8_flip_coin s2h_bf16_flip_coin"
# configs="nda_ddr4_disabled nda_ddr4 nda_ddr4_sync_to_host s2h_bf8_flip_coin s2h_bf8_flip_coin_indep"
# configs="nda_ddr4_disabled nda_ddr4_random_trans nda_ddr4_s2h_random_trans"
bench_home="/home/bcho/tmp/stampede_tmp"
configs=`ls $bench_home/mix8/workdir/replay_etrace`
mix_id=0
config=nda_ddr4

for i in {0..8}; do
  for cfg in $configs; do
    mix_id=$i
    config=$cfg
    stat_file="${bench_home}/mix${mix_id}/workdir/replay_etrace/${config}/stats.txt"
    collect
    # nda_bw
  done
done

