#!/bin/bash
 
gem5_home=/work/03528/bcho/stampede2/gem5
name=$1
# mem_config="DDR4-config"
mem_config=$2
app_name=`find ./benchmark/* -name "$name*" -exec sh -c 'echo $(basename {})' \;`

if [ -z $app_name ]; then
  echo "Application $app_name does not exist"
  exit 1
fi

if [ -z $mem_config ]; then
  echo "Specify memory configuration (e.g. DDR4-config)"
  exit 1
fi

cache_config="--caches --l2cache --cacheline_size=64 --l1d_size=32kB --l1d_assoc=4 --l2_size=512kB --l2_assoc=8 --l1i_size=32kB --l1i_assoc=4"
#ramulator_config="--mem-type=Ramulator --ramulator-config=./configs/ramulator/${mem_config}.cfg"
ramulator_config="--mem-type=Ramulator --ramulator-config=./test_config/${mem_config}.cfg"
outdir="$gem5_home/benchmark/$app_name/workdir/replay_etrace/$mem_config"
etrace_config="--data-trace-file=$gem5_home/benchmark/$app_name/workdir/m5out/system.switch_cpus.traceListener.deptrace.proto.gz --inst-trace-file=$gem5_home/benchmark/$app_name/workdir/m5out/system.switch_cpus.traceListener.fetchtrace.proto.gz"

cmd=`echo "./build/X86/gem5.opt --outdir=$outdir $gem5_home/configs/example/etrace_replay.py $etrace_config --mem-size=8GB --cpu-type=TraceCPU $cache_config $ramulator_config --maxinsts 500000000"`
echo $cmd
