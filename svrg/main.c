#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <gem5/m5ops.h>
#include <emmintrin.h>
#include <xmmintrin.h>
#include <smmintrin.h>
#include <immintrin.h>
#include <pthread.h>
#include <assert.h>
#include <stdint.h>
#include <unistd.h>

#define MAX_THREAD 4

#define AXPBY 0
#define AXPBYPCZ 1
#define AXPY 2
#define COPY 3
#define DOT 4
#define DOTC 5
#define GS 6
#define JACOBI 7
#define NRM2 8
#define SCAL 9
#define GEMV 10
#define XMY 11

#define M5OP_ADDR 0xFFFF0000

#define NORMAL 0
#define SSE 1
#define PSSE 2

#define NUM_EPOCH 5

void read_status();
unsigned int read_op();
void write_op(unsigned int val);

// write: 1 - launch one NDA op, 2 - launch all ops in the NDA queue
// read : 0 - Idle, 1 - Busy
volatile short* enable_nda = (short*)0xFFFFFF00;
// 0: AXPBY, 1: AXPBYPCZ, 2: AXPY, 3: COPY, 4: DOT, 5: DOTC, 6: GS, 7: JACOBI,
// 8: NRM2, 9: SCAL, 10: GEMV, 11: XMY, 12: COMM
volatile short* const nda_op = (short*)0xFFFFFF02;
volatile int* const nda_dim = (int*)0xFFFFFF04;
volatile int* const nda_sz = (int*)0xFFFFFF08;
volatile float* const nda_dst_base = (float*)0xFFFFFF0c;
volatile float* const nda_src1_base = (float*)0xFFFFFF10;
volatile float* const nda_src2_base = (float*)0xFFFFFF14;
volatile float* const nda_src3_base = (float*)0xFFFFFF18;
volatile int* const nda_pkt_end = (int*)0xFFFFFF1C;

void *m5_mem = NULL;
int nda_gran = 4;

typedef struct {
    uint64_t pfn : 54;
    unsigned int soft_dirty : 1;
    unsigned int file_page : 1;
    unsigned int swapped : 1;
    unsigned int present : 1;
} PagemapEntry;

/* Parse the pagemap entry for the given virtual address.
 *
 * @param[out] entry      the parsed entry
 * @param[in]  pagemap_fd file descriptor to an open /proc/pid/pagemap file
 * @param[in]  vaddr      virtual address to get entry for
 * @return 0 for success, 1 for failure
 */
int pagemap_get_entry(PagemapEntry *entry, int pagemap_fd, uintptr_t vaddr)
{
    size_t nread;
    ssize_t ret;
    uint64_t data;
    uintptr_t vpn;

    vpn = vaddr / sysconf(_SC_PAGE_SIZE);
    nread = 0;
    while (nread < sizeof(data)) {
        ret = pread(pagemap_fd, &data, sizeof(data) - nread,
                vpn * sizeof(data) + nread);
        nread += ret;
        if (ret <= 0) {
            return 1;
        }
    }
    entry->pfn = data & (((uint64_t)1 << 54) - 1);
    entry->soft_dirty = (data >> 54) & 1;
    entry->file_page = (data >> 61) & 1;
    entry->swapped = (data >> 62) & 1;
    entry->present = (data >> 63) & 1;
    return 0;
}

/* Convert the given virtual address to physical using /proc/PID/pagemap.
 *
 * @param[out] paddr physical address
 * @param[in]  pid   process to convert for
 * @param[in] vaddr virtual address to get entry for
 * @return 0 for success, 1 for failure
 */
int virt_to_phys_user(uintptr_t *paddr, pid_t pid, uintptr_t vaddr)
{
    char pagemap_file[BUFSIZ];
    int pagemap_fd;

    snprintf(pagemap_file, sizeof(pagemap_file), "/proc/%ju/pagemap", (uintmax_t)pid);

    pagemap_fd = open(pagemap_file, O_RDONLY);
    if (pagemap_fd < 0) {
        return 1;
    }
    PagemapEntry entry;
    if (pagemap_get_entry(&entry, pagemap_fd, vaddr)) {
        return 1;
    }
    close(pagemap_fd);
    *paddr = (entry.pfn * sysconf(_SC_PAGE_SIZE)) + (vaddr % sysconf(_SC_PAGE_SIZE));
    return 0;
}

void map_m5_mem()
{
#ifdef SIMULATION
    int fd;

    fd = open("/dev/mem", O_RDWR | O_SYNC);
    if (fd == -1) {
        perror("Can't open /dev/mem");
        exit(1);
    }

    m5_mem = mmap(NULL, 0x10000, PROT_READ | PROT_WRITE, MAP_SHARED, fd,
                  M5OP_ADDR);
    if (!m5_mem) {
        perror("Can't mmap /dev/mem");
        exit(1);
    }
    printf("[user program] m5op region mmapped.\n");
#else
    printf("Nothing to do for map_m5_mem when executing on the host computer\n");
#endif
}

void print_usage(char** argv) {
  printf(
      "[Usage]: %s <app> <num_features> <num_samples> <epoch> <accel_factor> "
      "<blocking>\n"
      "-> op: 0(svrg), 1(test normal axpy), 2(test sse axpy), 3(test pthread sse axpy)\n",
      argv[0]);
}

float dot(float* a, float* b, long dim) {
  float result = 0;
  for (int i = 0; i < dim; ++i) result += a[i] * b[i];

  return result;
}



float access_member(__m128 v, int index) {
  union vec { 
    __m128 sse; 
    float f[4];
  } converter;
  converter.sse = v;
  return converter.f[index];
}

float sse_dot(float* src1, float* src2, long dim) {
  float sum = 0;
  __m128 tmp = _mm_set_ps(0, 0, 0, 0);

  for (int i = 0; i < dim; i += 4) {
    __m128 s1 = _mm_load_ps(&src1[i]);
    __m128 s2 = _mm_load_ps(&src2[i]);
    tmp = _mm_dp_ps(s1, s2, 1);
    sum += access_member(tmp, 0);
  }
  return sum;
}

void axpby(float* dst, float* src1, float* src2, long dim) {
  for (int i = 0; i < dim; ++i) dst[i] = src1[i] + src2[i];  // assuming fma
}

void sse_axpby(float* dst, float* src1, float* src2, long dim) {
  for (int i = 0; i < dim; i += 4) {
    __m128 s1 = _mm_load_ps(&src1[i]);
    __m128 s2 = _mm_load_ps(&src2[i]);
    s1 = _mm_add_ps(s1, s2);
    _mm_store_ps(&dst[i], s1);
  }
}

void sse_axpy(float* dst, float* src, long dim) {
  for (int i = 0; i < dim; i += 4) {
    __m128 s = _mm_load_ps(&src[i]);
    __m128 d = _mm_load_ps(&dst[i]);
    d = _mm_add_ps(s, d);
    _mm_store_ps(&dst[i], d);
  }
}

void sse_xmy(float* dst, float* src, long dim) {
  for (int i = 0; i < dim; i += 4) {
    __m128 s = _mm_load_ps(&src[i]);
    __m128 d = _mm_load_ps(&dst[i]);
    d = _mm_mul_ps(s, d);
    _mm_store_ps(&dst[i], d);
  }
}

void sse_gemv(float* vo, float* M, float* vi, long dim, long num_samples) {
  for (int i = 0; i < num_samples; ++i) {
    int idx = i * dim;
    vo[i] = sse_dot(&M[idx], vi, dim);
  }
}

void copy_host2nda(float* src, float* dst, long dim) {
  for (int i = 0; i < dim; i += 4) {
    __m128 val = _mm_load_ps(&src[i]);
    _mm_stream_ps(&dst[i], val);
  }
}

void copy_nda2host(float* src, float* dst, long dim) {
  for (int i = 0; i < dim; i += 4) {
    __m128i val_i = _mm_stream_load_si128((__m128i *)&src[i]);
    __m128 val = _mm_castsi128_ps(val_i);
    _mm_store_ps(&dst[i], val);
  }
}

typedef struct {
  int id;
  float *src;
  float *dst;
  long dim;
} Args;

void* pthread_worker(void* args) {
  Args *arg_ptr = (Args *)args;
  int id = arg_ptr->id;
  float* src = arg_ptr->src;
  float* dst = arg_ptr->dst;
  long dim = arg_ptr->dim;
  
  sse_axpy(dst, src, dim);
  printf("[pthread] worker %d done\n", id);
}

void pthread_sse_axpy(float *dst, float* src, long dim) {
  assert(dim % MAX_THREAD == 0);

  pthread_t threads[MAX_THREAD];
  Args args[MAX_THREAD];

  for (int i = 0; i < MAX_THREAD; i++) {
    args[i].id = i;
    args[i].src = src + i * (dim / MAX_THREAD);
    args[i].dst = dst + i * (dim / MAX_THREAD);
    args[i].dim = dim / MAX_THREAD;
    pthread_create(&threads[i], NULL, pthread_worker, &args[i]);
  }

  for (int i = 0; i < MAX_THREAD; i++) 
    pthread_join(threads[i], NULL);
}

void axpy(float* dst, float* src, long dim) {
  for (int i = 0; i < dim; ++i) dst[i] += src[i];  // assuming fma
}

void sse_copy(float* dst, float* src, long dim) {
  for (int i = 0; i < dim; i += 4) {
    __m128 s = _mm_load_ps(&src[i]);
    _mm_store_ps(&dst[i], s);
  }
}

void copy(float* dst, float* src, long dim) {
  for (int i = 0; i < dim; ++i) dst[i] = src[i];
}

void sse_scale(float* src, long dim) {
  const char imm = 2; // just arbitrary value
  __m128 imm_v = _mm_set_ps(imm, imm, imm, imm);
  for (int i = 0; i < dim; i += 4) {
    __m128 s = _mm_load_ps(&src[i]);
    s = _mm_mul_ps(s, imm_v);
    _mm_store_ps(&src[i], s);
  }
}

void scale(float* src, long dim) {
  for (int i = 0; i < dim; ++i) src[i] = 2 * src[i];
}

uintptr_t physical_addr(uintptr_t vaddr) {
  uintptr_t paddr;
  virt_to_phys_user(&paddr, getpid(), vaddr);
  // paddr = vaddr;

  return paddr;
}

void set_nda_reg(int opcode, uintptr_t dst, uintptr_t src1, uintptr_t src2, long dim, long num_samples) {
#ifdef SIMULATION
  *nda_op = opcode;
  *nda_dim = dim;
  *nda_sz = num_samples;
  *nda_dst_base = physical_addr(dst);
  *nda_src1_base = physical_addr(src1);
  *nda_src2_base = physical_addr(src2);
  *nda_src3_base = 0;
  *nda_pkt_end = 1;
  _mm_sfence();
#endif
}

void read_nda_reg() {
#ifdef SIMULATION
  printf("NDA OP: %d\n", *nda_op);
  printf("NDA DIM: %d\n", *nda_dim);
  printf("NDA SZ: %d\n", *nda_sz);
  printf("NDA dst base: 0x%jx\n", (uintptr_t)*nda_dst_base);
  printf("NDA src1 base: 0x%jx\n", (uintptr_t)*nda_src1_base);
  printf("NDA src2 base: 0x%jx\n", (uintptr_t)*nda_src2_base);
  printf("NDA src3 base: 0x%jx\n", (uintptr_t)*nda_src3_base);
#endif
}

void count(long cnt) {
  while(cnt > 0)
    --cnt;
}

void launch_nda_op(int opcode, uintptr_t dst, uintptr_t src1, uintptr_t src2, long dim, long num_samples,
              int blocking) {
#ifdef SIMULATION
  set_nda_reg(opcode, dst, src1, src2, dim, num_samples);
  if (blocking) {
    *enable_nda = 1;
    while (*enable_nda)
      count(100000000);
  }
#endif
}

long nda_offset = 0;
float* nda_malloc(long size, long* nda_base) {
  *nda_base = nda_offset;
  nda_offset += size;
  return (float*)malloc(size);
}

void perf_test_axpy(long dim, int num_iter, char mode) {
  long base_addr = 0;
  float* a = (float*)nda_malloc(sizeof(float) * dim, &base_addr);
  uintptr_t a_base = base_addr;
  float* b = (float*)nda_malloc(sizeof(float) * dim, &base_addr);
  uintptr_t b_base = base_addr;
  
  printf("--- Performance test (AXPY) ---\n");
  printf("--> Testing with CPU\n");

#ifdef SIMULATION
  m5_reset_stats(0, 0);
#endif
  switch (mode) {
    case 0:
      for(int i = 0; i < num_iter; i++)
        axpy(a, b, dim);
      break;
    case 1:
      for(int i = 0; i < num_iter; i++)
        sse_axpy(a, b, dim);
      break;
    case 2:
      for(int i = 0; i < num_iter; i++)
        pthread_sse_axpy(a, b, dim);
      break;
  }
#ifdef SIMULATION
  m5_dump_reset_stats(0, 0);

  // printf("--> Testing with NDA\n");
  // m5_reset_stats(0, 0);
  // for(int i = 0; i < num_iter; i++)
  //   launch_nda_op(AXPY, b_base, a_base, 0, dim, 0, 1);
  // m5_dump_reset_stats(0, 0);
#endif

  printf("Done\n");

  free(b);
  free(a);
}

void perf_test(int op, long dim, long num_samples, int num_iter, char mode) {
  switch(op) {
    case AXPY:
      perf_test_axpy(dim, num_iter, mode); return;

    default:
      printf("[Error] Test case for op %d does not exist.\n", op);
      exit(1);
  }
}

void host_svrg(long dim, long num_samples, int epoch) {
  float* X = (float*)malloc(sizeof(float) * num_samples * dim);
  float* y = (float*)malloc(sizeof(float) * dim);
  float* w = (float*)malloc(sizeof(float) * dim);
  float* w_tilde = (float*)malloc(sizeof(float) * dim);
  float* grad_w = (float*)malloc(sizeof(float) * dim);
  float* grad_w_tilde = (float*)malloc(sizeof(float) * dim);
  float* mu = (float*)malloc(sizeof(float) * dim);
  float* tmp = (float*)malloc(sizeof(float) * dim);
  float* tmp1 = (float*)malloc(sizeof(float) * dim);

  for (int i = 0; i < NUM_EPOCH; ++i) {
    /////////////////////////////////////////////////
    // compute the average of batch gradient (HOST) //
    /////////////////////////////////////////////////
    printf("\n--- Iteration %d -----------------------\n", i); 
    printf("[HOST] Computing average of batch gradient\n"); 
    sse_gemv(tmp1, X, w, dim, num_samples);
    sse_xmy(tmp1, y, num_samples);
    sse_xmy(tmp1, y, num_samples);
    for (int i = 0; i < num_samples; ++i) {
      int idx = i * dim;
      sse_axpy(mu, &X[idx], dim);
    }

    //////////////////////////
    // main training (HOST) //
    //////////////////////////
    printf("[HOST] Starting SGD steps\n");

    for (int j = 0; j < epoch; ++j) {
      int idx = rand() % num_samples;
      idx *= dim;

      /** compute grad_w **/
      sse_dot(&X[idx], w, dim);
      // omit scalar computation
      sse_copy(grad_w, &X[idx], dim);  // equals to "scale and write"

      /*** compute grad_w_tilde ***/
      sse_dot(&X[idx], w_tilde, dim);
      // omit scalar computation
      sse_copy(grad_w_tilde, &X[idx], dim);

      /*** compute gradient term ***/
      sse_axpby(tmp, grad_w, grad_w_tilde, dim);
      sse_axpy(tmp, mu, dim);

      /*** update w ***/
      sse_axpy(w, tmp, dim);
    }
  }

  // free up memory spaces
  free(tmp1);
  free(tmp);
  free(mu);
  free(grad_w_tilde);
  free(grad_w);
  free(w);
  free(y);
  free(X);
}

void parallel_svrg(long dim, long num_samples, int epoch, int accel_factor) {
  long base_addr = 0;
  float* X = (float*)nda_malloc(sizeof(float) * num_samples * dim, &base_addr);
  uintptr_t X_base = base_addr;

  float* y = (float*)nda_malloc(sizeof(float) * dim, &base_addr);
  uintptr_t y_base = base_addr;

  float* w_nda = (float*)nda_malloc(sizeof(float) * dim, &base_addr);
  uintptr_t w_nda_base = base_addr;

  float* mu_nda = (float*)nda_malloc(sizeof(float) * dim, &base_addr);
  uintptr_t mu_nda_base = base_addr;

  float* tmp1 = (float*)nda_malloc(sizeof(float) * dim, &base_addr);
  uintptr_t tmp1_base = base_addr;

  float* w = (float*)nda_malloc(sizeof(float) * dim, &base_addr);
  float* w_tilde = (float*)nda_malloc(sizeof(float) * dim, &base_addr);
  float* grad_w = (float*)nda_malloc(sizeof(float) * dim, &base_addr);
  float* grad_w_tilde = (float*)nda_malloc(sizeof(float) * dim, &base_addr);
  float* mu = (float*)nda_malloc(sizeof(float) * dim, &base_addr);
  float* tmp = (float*)nda_malloc(sizeof(float) * dim, &base_addr);

  /////////////////////////////////////////////////
  // compute the average of batch gradient (NDA) //
  /////////////////////////////////////////////////
  // TODO: vaddr --> paddr

#ifdef SIMULATION
   long acc_num_samp = num_samples / accel_factor;
   long acc_dim = dim / accel_factor;
   launch_nda_op(GEMV, tmp1_base, X_base, w_nda_base, dim, acc_num_samp, 0);
   launch_nda_op(XMY, tmp1_base, y_base, 0, acc_num_samp, 0, 0);
   launch_nda_op(XMY, tmp1_base, y_base, 0, acc_num_samp, 0, 0);
   for (int i = 0; i < num_samples / accel_factor; ++i) {
     int idx = i * dim;
     launch_nda_op(AXPY, mu_nda_base, X_base + sizeof(float) * idx, 0, acc_dim, 0, 0);
   }

  //long acc_num_samp = num_samples / accel_factor;
  //long acc_dim = dim / accel_factor;
  //launch_nda_op(GEMV, tmp1_base, X, w_nda, dim, acc_num_samp, 0);
  //launch_nda_op(XMY, tmp1, y, 0, acc_num_samp, 0, 0);
  //launch_nda_op(XMY, tmp1, y, 0, acc_num_samp, 0, 0);
  //for (int i = 0; i < num_samples / accel_factor; ++i) {
  //  int idx = i * dim;
  //  launch_nda_op(AXPY, mu_nda_base, X[idx], 0, acc_dim, 0, 0);

  printf("Launching NDA operations in asynchronous mode\n");
  *enable_nda = 3; // taking a snapshot of current packets and 
                   // execute NDA operations in non-blocking manner
#endif

  //////////////////////////
  // main training (HOST) //
  //////////////////////////
  int num_update = 0;
  const int print_period = 100;
  for (int i = 0; i < epoch * NUM_EPOCH; ++i) {
    if (i % epoch == 0)
      printf("\n--- Iteration %d -----------------------\n", i/epoch); 

    if (i % print_period == 0)
      printf("\n  --- Step %d -----------------------\n", i); 

    int idx = rand() % num_samples;
    idx *= dim;

    /** compute grad_w **/
    sse_dot(&X[idx], w, dim);
    // omit scalar computation
    sse_copy(grad_w, &X[idx], dim);  // equals to "scale and write"

    /*** compute grad_w_tilde ***/
    sse_dot(&X[idx], w_tilde, dim);
    // omit scalar computation
    sse_copy(grad_w_tilde, &X[idx], dim);

    /*** compute gradient term ***/
    sse_axpby(tmp, grad_w, grad_w_tilde, dim);
    sse_axpy(tmp, mu, dim);

    /*** update w ***/
    sse_axpy(w, tmp, dim);

#ifdef SIMULATION
    if (!*enable_nda) {
      ++num_update;
      printf("NDA done. \nUpdating the correction terms (%d)", num_update);
      copy_nda2host(mu_nda, mu, dim);
      copy_host2nda(w, w_nda, dim);
      *enable_nda = 2; // since the asynchronous mode automatically restores
                       // the snapshot after all the NDA operations are executed, 
                       // we just execute in non-blocking mode from now on
    }
#endif
  }

  // uintptr_t paddr = physical_addr((uintptr_t)tmp1);
  // printf("vaddr = 0x%jx\n", (uintmax_t)tmp1);
  // printf("paddr = 0x%jx\n", paddr);

  // free up memory spaces
  free(tmp1);
  free(tmp);
  free(mu_nda);
  free(mu);
  free(grad_w_tilde);
  free(grad_w);
  free(w_nda);
  free(w);
  free(y);
  free(X);

}

void svrg(long dim, long num_samples, int epoch, int accel_factor, int blocking) {
  if (blocking)
    printf("Blocking mode\n");
  else
    printf("Nonblocking mode \n");

  long base_addr = 0;
  float* X = (float*)nda_malloc(sizeof(float) * num_samples * dim, &base_addr);
  float X_base = base_addr;

  float* y = (float*)nda_malloc(sizeof(float) * dim, &base_addr);
  float y_base = base_addr;

  float* w_nda = (float*)nda_malloc(sizeof(float) * dim, &base_addr);
  float w_nda_base = base_addr;

  float* mu_nda = (float*)nda_malloc(sizeof(float) * dim, &base_addr);
  float mu_nda_base = base_addr;

  float* tmp1 = (float*)nda_malloc(sizeof(float) * dim, &base_addr);
  float tmp1_base = base_addr;

  float* w = (float*)nda_malloc(sizeof(float) * dim, &base_addr);
  float* w_tilde = (float*)nda_malloc(sizeof(float) * dim, &base_addr);
  float* grad_w = (float*)nda_malloc(sizeof(float) * dim, &base_addr);
  float* grad_w_tilde = (float*)nda_malloc(sizeof(float) * dim, &base_addr);
  float* mu = (float*)nda_malloc(sizeof(float) * dim, &base_addr);
  float* tmp = (float*)nda_malloc(sizeof(float) * dim, &base_addr);

  for (int i = 0; i < NUM_EPOCH; ++i) {
    /////////////////////////////////////////////////
    // compute the average of batch gradient (NDA) //
    /////////////////////////////////////////////////
    // TODO: vaddr --> paddr
    printf("\n--- Iteration %d -----------------------\n", i); 
    printf("[NDA] Computing average of batch gradient\n"); 
#ifdef SIMULATION
    long acc_num_samp = num_samples / accel_factor;
    long acc_dim = dim / accel_factor;
    launch_nda_op(GEMV, tmp1_base, X_base, w_nda_base, dim, acc_num_samp, blocking);
    launch_nda_op(XMY, tmp1_base, y_base, 0, acc_num_samp, 0, blocking);

    if (blocking) 
      scale(tmp1, dim); // replacing sigmoid funcion with scale

    launch_nda_op(XMY, tmp1_base, y_base, 0, acc_num_samp, 0, blocking);

    for (int i = 0; i < num_samples / accel_factor; ++i) {
      int idx = i * dim;
      launch_nda_op(AXPY, mu_nda_base, X_base + sizeof(float) * idx, 0, acc_dim, 0, blocking);
    }

    if (!blocking) {
      printf("Launching NDA operations in non-blocking mode\n");
      *enable_nda = 2;
    } else {  
      printf("NDA done! \nCopying mu from NDA to HOST in blocking mode\n");
      copy_nda2host(mu_nda, mu, dim);
    }
#endif

    /////////////////////////
    // main training (HOST) //
    /////////////////////////
    printf("[HOST] Starting SGD steps\n");

    for (int j = 0; j < epoch; ++j) {
      int idx = rand() % num_samples;
      idx *= dim;

      /** compute grad_w **/
      sse_dot(&X[idx], w, dim);
      // omit scalar computation
      sse_copy(grad_w, &X[idx], dim);  // equals to "scale and write"

      /*** compute grad_w_tilde ***/
      sse_dot(&X[idx], w_tilde, dim);
      // omit scalar computation
      sse_copy(grad_w_tilde, &X[idx], dim);

      /*** compute gradient term ***/
      sse_axpby(tmp, grad_w, grad_w_tilde, dim);
      sse_axpy(tmp, mu, dim);

      /*** update w ***/
      sse_axpy(w, tmp, dim);
    }

#ifdef SIMULATION
    if (!blocking) {
      printf("HOST done! \nCopying mu from NDA to HOST in non-blocking mode\n");
      copy_nda2host(mu_nda, mu, dim);

      printf("Waiting for NDAs\n");
      while (*enable_nda)
        count(100000000);
      printf("NDA done\n");
    }

    printf("Copying w from HOST to NDA\n");
    copy_host2nda(w, w_nda, dim);
#endif
  }

  // free up memory spaces
  free(tmp1);
  free(tmp);
  free(mu_nda);
  free(mu);
  free(grad_w_tilde);
  free(grad_w);
  free(w_nda);
  free(w);
  free(y);
  free(X);
}

int main(int argc, char** argv) {
  if (argc < 7) {
    print_usage(argv);
    exit(1);
  }

  int app = atoi(argv[1]);
  long dim = atoi(argv[2]);
  long num_samples = atoi(argv[3]);
  int epoch = atoi(argv[4]); // number of steps per epoch
  int accel_factor = atoi(argv[5]);
  int blocking = atoi(argv[6]);
  nda_gran = 4; // in KB
  if (argc >= 8)
    nda_gran = atoi(argv[7]);

#ifdef SIMULATION
  map_m5_mem();
#endif

  switch(app) {
    case 0: // svrg
#ifdef SIMULATION
      m5_reset_stats(0, 0);
#endif
      svrg(dim, num_samples, epoch, accel_factor, blocking); 
#ifdef SIMULATION
      m5_dump_reset_stats(0, 0);
#endif
      break;

    case 1: // perf test (AXPY, normal)
      perf_test(AXPY, dim, num_samples, epoch, NORMAL); break;

    case 2: // perf test (AXPY, sse)
      perf_test(AXPY, dim, num_samples, epoch, SSE); break;

    case 3: // perf test (AXPY, sse, pthread)
      perf_test(AXPY, dim, num_samples, epoch, PSSE); break;

    case 4: // parallel SVRG
#ifdef SIMULATION
      m5_reset_stats(0, 0);
#endif
      parallel_svrg(dim, num_samples, epoch, accel_factor); 
#ifdef SIMULATION
      m5_dump_reset_stats(0, 0);
#endif
      break;

    case 5: // host-only SVRG
#ifdef SIMULATION
      m5_reset_stats(0, 0);
#endif
      host_svrg(dim, num_samples, epoch); 
#ifdef SIMULATION
      m5_dump_reset_stats(0, 0);
#endif
      break;

    default:
      printf("[Error] Undefined application, %d", app);
      break;
  }

  return 0;
}
